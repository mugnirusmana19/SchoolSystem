@extends('layouts/master')

@section('content')
<div class="body">

  <div role="main" class="main shop">
    <section class="page-header">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>Tentang Kami</h1>
          </div>
        </div>
      </div>
    </section>
    <div class="container">

      <form id="contactForm" action="" method="POST">

        <div class="col-md-6">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12">
                <label>Nama Lengkap</label>
                <input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" id="subject" required>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="form-group">
              <div class="col-md-12">
                <label>Email</label>
                <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="form-group">
              <div class="col-md-12">
                <label>Deskripsi</label>
                <textarea maxlength="5000" data-msg-required="Please enter your message." rows="10" class="form-control" name="message" id="message" required></textarea>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <input type="submit" value="Kirim Pesan" class="btn btn-block btn-primary btn-lg" data-loading-text="Loading...">
            </div>
          </div>
        </div>

        <div class="col-md-6">

          <h4 class="heading-primary mt-lg">Tentang <strong>Kami</strong></h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

          <hr>

          <h4 class="heading-primary">Kontak <strong>Kami</strong></h4>
          <ul class="list list-icons list-icons-style-3 mt-xlg">
            <li><i class="fa fa-map-marker"></i> <strong>Address:</strong> 1234 Street Name, City Name, United States</li>
            <li><i class="fa fa-phone"></i> <strong>Phone:</strong> (123) 456-789</li>
            <li><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a></li>
          </ul>

        </div>

      </form>

    </div>
  </div>

</div>
@endsection
