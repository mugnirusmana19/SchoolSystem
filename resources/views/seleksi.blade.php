@extends('layouts/master')

@section('content')
<div class="body">

  <div role="main" class="main shop">
    <section class="page-header">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>HASIL SELEKSI PESETA DIDIK</h1>
          </div>
        </div>
      </div>
    </section>
    <div class="container">

      <table class="table">
        <thead>
          <tr>
            <th width="1%"><center>No</center></th>
            <th width="15%"><center>NISN</center></th>
            <th width="74%">NAMA</th>
            <th width="10%"><center>NA</center></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><center>1</center></td>
            <td><center>1234567890</center></td>
            <td>Ade Mugni Rusmana</td>
            <td><center>297.000</center></td>
          </tr>
          <tr>
            <td><center>2</center></td>
            <td><center>1234567891</center></td>
            <td>Abdul Azis Agung Riyadi</td>
            <td><center>296.000</center></td>
          </tr>
          <tr>
            <td><center>3</center></td>
            <td><center>1234567892</center></td>
            <td>Sigit Stepanus Sitepu</td>
            <td><center>295.000</center></td>
          </tr>
          <tr>
            <td><center>4</center></td>
            <td><center>1234567893</center></td>
            <td>Gunawan Chandro Fericson</td>
            <td><center>294.000</center></td>
          </tr>
        </tbody>
      </table>

    </div>
  </div>

</div>
@endsection
