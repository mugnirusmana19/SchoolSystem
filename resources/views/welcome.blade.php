@extends('layouts/master')

@section('content')
  <div role="main" class="main">

    <div role="main" class="main shop">

      <div class="container">

        <div class="row">
          <div class="col-md-12">
            <hr class="tall">
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <strong>Pendaftaran Calon Siswa Baru Tahun Ajaran 2017 Telah Dibuka</strong>
            <p>Ayo daftar sekarang!</p>
            <img src="{{asset('/porto/img/PPDB.jpg')}}" width="100%">
          </div>
        </div>

      </div>

    </div>

  </div>
@endsection
