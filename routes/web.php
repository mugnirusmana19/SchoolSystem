<?php

Route::get('/', function () {
    return view('welcome');
});
Route::get('/pendaftaran', 'PendaftaranController@index');
Route::get('/seleksi', 'SeleksiController@index');
Route::get('/tentang', 'TentangController@index');
